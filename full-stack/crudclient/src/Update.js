import React,{Component} from 'react';
import logo from './logo.svg';
import './App.css';

class Update extends Component {
 
  constructor(props){
    super(props);
    this.state = {product : {}}
  }

  onIdChange = (e)=>{
    this.setState({id:e.target.value});
  }

  onNameChange = (e)=>{
    this.setState({name:e.target.value});
  }

  onDecriptionChange = (e)=>{
    this.setState({description:e.target.value});
  }

  onPriceChange = (e)=>{
    this.setState({price:e.target.value});
  }

  updateProduct(event){
    const axios = require('axios');
     axios.put('http://localhost:8090/api/products/',{
       id : this.state.id,
       name : this.state.name,
       price : this.state.price
     })
     .then(res=>{
      console.log(res.data);
     }).catch(error=>{
       console.error("error",error);
     })
  }

 render(){
  return (
    <div>
      Enter Product Id :<input onChange={this.onIdChange}/> <br/>
      Enter Product Name :<input onChange={this.onNameChange}/> <br/>
      Enter Product Price :<input onChange={this.onPriceChange}/> <br/>
      <button onClick={this.updateProduct.bind(this)}>Update Product</button>
    </div>
  );
 }
  
}

export default Update;
