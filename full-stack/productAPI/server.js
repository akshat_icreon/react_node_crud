var express = require("express");
var bodyParser = require("body-parser");
var app = express();
var productAPI = require("./controllers/product.controller");

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use("/api/products",productAPI);
app.listen(8090);
console.log("Application is running on port 8090");
