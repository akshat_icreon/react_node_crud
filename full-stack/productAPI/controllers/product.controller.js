var dbCon = require("../config/db_connection");
var connection = dbCon.getConnection();
connection.connect();
var express = require("express");
var router = express.Router();

router.get("/",(req,res)=>{
    var fetchAll = "SELECT * FROM product";
    connection.query(fetchAll,(err, records,fields)=>{
        if(err){
            console.error("Error occurs while fetching Records");
        }else{
            res.send(records);
        }
    })
})

router.get("/:id",(req,res)=>{
    var fetchById = "SELECT * FROM product WHERE id="+req.params.id;
    connection.query(fetchById,(err, records,fields)=>{
        if(err){
            console.error("Error occurs while fetching Records");
        }else{
            res.send(records);
        }
    })
})

router.post("/",(req,res)=>{
    var id = req.body.id;
    var name = req.body.name;
    var description = req.body.description;
    var price = req.body.price;

    var insertProduct = "INSERT INTO product values ("+id+",'"+name+"','"+description+"','"+price+",')";
    connection.query(insertProduct,(err, records)=>{
        if(err){
            console.error("Error occurs while inserting Records");
        }else{
            res.send({insert:"success"});
        }
    })
})

router.put("/",(req,res)=>{
    var id = req.body.id;
    var name = req.body.name;
    var price = req.body.price;

    var updateProduct = "UPDATE product SET name='"+name+"',price='"+price+",' WHERE id="+id;
    connection.query(updateProduct,(err, records)=>{
        if(err){
            console.error("Error occurs while updating Records");
        }else{
            res.send({update:"success"});
        }
    })
})

router.delete("/:id",(req,res)=>{
    var fetchById = "DELETE FROM product WHERE id="+req.params.id;
    connection.query(fetchById,(err, records,fields)=>{
        if(err){
            console.error("Error occurs while fetching Records");
        }else{
            res.send({delete:"success"});
        }
    })
})

module.exports = router;