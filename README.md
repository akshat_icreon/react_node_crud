# README #

Backend API's using node 
API sample - localhost:8090/api/products/

### Node Dependency ###
* yarn add express --save
* yarn add mysql --save
* yarn add body-parser --save

### React Dependency ###
* yarn add axios
* npm install --save react-router-dom


### How do I get set up? ###

use node_db;
create table product(id int, name varchar(255), description varchar(255), price varchar(255));

node server.js


